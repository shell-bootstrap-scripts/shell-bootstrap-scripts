These scripts do not call each other. Rather, each file textually includes everything it needs.
This is so that you can always get everything you need with a single `wget --no-proxy https://gitlab.com/shell-bootstrap-scripts/shell-bootstrap-scripts/-/raw/master/fix_all_gotchas.sh` or `curl`.

Because of this, these scripts should not be edited directly.
These scripts are generated from the cookiecutter.

