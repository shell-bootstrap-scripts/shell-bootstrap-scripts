# If we need to apk add openssh-client, then we will need HTTPS_PROXY set first.
# This potentially leads to a problem if we need SSH to access the ETC_ENVIRONMENT_LOCATION.
# The ETC_ENVIRONMENT_LOCATION is not generally intended for secret keys like the SSH_PRIVATE_DEPLOY_KEY.

if [ -z ${ETC_ENVIRONMENT_LOCATION+ABC} ]; then
echo "ETC_ENVIRONMENT_LOCATION is unset"
ETC_ENVIRONMENT_LOCATION=$CI_SERVER_URL/shell-bootstrap-scripts/network-settings/-/raw/master/set_variables_in_CI.sh
fi
ls set_variables_in_CI.sh || wget $ETC_ENVIRONMENT_LOCATION
ls set_variables_in_CI.sh

